# Go server #

> Start go with gin.

## go usage ##

```bash
#run
go run index.go

#build
go build
```

### Installation ###

On MAC [download](https://golang.org/doc/install?download=go1.9.2.darwin-amd64.pkg)

### Set path ###

```bash
cd /usr/local/go

# search
./go env

GOARCH="amd64"
GOBIN=""
GOEXE=""
GOHOSTARCH="amd64"
GOHOSTOS="darwin"
GOOS="darwin"
GOPATH="/Users/taylorpzreal/go"
GORACE=""
GOROOT="/usr/local/go"
GOTOOLDIR="/usr/local/go/pkg/tool/darwin_amd64"
GCCGO="gccgo"
CC="clang"
GOGCCFLAGS="-fPIC -m64 -pthread -fno-caret-diagnostics -Qunused-arguments -fmessage-length=0 -fdebug-prefix-map=/var/folders/gv/21zd0n217k513jrbdqc5dm_40000gn/T/go-build615205357=/tmp/go-build -gno-record-gcc-switches -fno-common"
CXX="clang++"
CGO_ENABLED="1"
CGO_CFLAGS="-g -O2"
CGO_CPPFLAGS=""
CGO_CXXFLAGS="-g -O2"
CGO_FFLAGS="-g -O2"
CGO_LDFLAGS="-g -O2"
PKG_CONFIG="pkg-config"

# SET GOROOT & GOBIN
vim ~/.bash_profile

  export GOROOT=/usr/local/go
  export GOBIN=${GOROOT}/bin
  export PATH=${PATH}:${GOBIN}

:wq

source .bash_profile

# ok
```

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

## Plugins ##

* dep - Go dependency management tool

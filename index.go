package main

import "fmt"
import "github.com/gin-gonic/gin"
import "net/http"

type Person struct {
	Name string
	City string
	Company string
}

type Attendee struct {
	Topic string
	Person Person
}

func main() {
	router := gin.Default()

	router.GET("/ping", func(c *gin.Context) {
		// c.String(200, "pong")
		// fmt.Printf(http.StatusOK)
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})

	fmt.Printf("hello, world\n")

	router.Run(":4000")
}